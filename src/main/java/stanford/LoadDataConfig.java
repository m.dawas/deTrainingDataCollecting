package stanford;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import it.unimi.dsi.fastutil.io.FastBufferedInputStream;

public class LoadDataConfig {

	// {{Final fields
	private static final String DEFAULT_CONFIG = "conf/config.properties";
	private static final String DBSERVER_ADDRESS = "server.ip";
	private static final String DBSERVER_PORT = "server.port";
	private static final String DBSERVER_WIKIPEDIADBNAME = "server.wikipediaDbName";
	private static final String DBSERVER_WIKIDATADBNAME = "server.wikidataDbName";
	private static final String DBSERVER_SENTCOLLNAME = "server.sentencesCollection";
	private static final String DBSERVER_ANNOTCOLLNAME = "server.annotationsCollection";
	private static final String DBSERVER_DOCCOLLNAME = "server.documentsCollection";
	private static final String WORKFLOW_REQUIREDCLASSES = "requiredClasses";

	private static final String DOCUMENT_DOCID = "column.documents.docId";
	private static final String DOCUMENT_TITLE = "column.documents.title";
	private static final String DOCUMENT_CONTENT = "column.documents.content";

	private static final String SENTENCE_DOCID = "column.sentences.docId";
	private static final String SENTENCE_SENTID = "column.sentences.senId";
	private static final String SENTENCE_STARTPOSITION = "column.sentences.sentenceStart";
	private static final String SENTENCE_ENDPOSITION = "column.sentences.sentenceEnd";
	private static final String SENTENCE_TEXT = "column.sentences.sentenceText";

	private static final String ANNOTATION_DOCID = "column.annotations.docId";
	private static final String ANNOTATION_SENTID = "column.annotations.senId";
	private static final String ANNOTATION_STARTPOSITION = "column.annotations.tokenStart";
	private static final String ANNOTATION_ENDPOSITION = "column.annotations.tokenEnd";
	private static final String ANNOTATION_ENTITYCLASS = "column.annotations.entityClass";
	private static final String ANNOTATION_CONFIDENCE = "column.annotations.confidence";

	private static final String OUTPUT_FILEDIRECTORY = "output.directory";
	private static final String OUTPUT_TESTFILEDIRECTORY = "output.testDirectory";
	private static final String OUTPUT_FILENAME = "output.filename";
	private static final String OUTPUT_TESTFILENAME = "output.testfilename";
	private static final String OUTPUT_WRITEALLSENTENCES = "output.writeAllSentences";
	private static final String OUTPUT_MAXDOCCOUNT = "output.maxDocCount";
	private static final String OUTPUT_TESTDOCSNUM = "output.numTestDocs";

	private static final String OPT_ANNOTPERSENT = "optimization.numOfAnnotPerSent";
	private static final String OPT_CONFLEVEL = "optimization.confidenceLevel";
	private static final String OPT_ISUPPERCASE = "optimization.isUpperCase";
	
	private static final String WD_INFERREDTYPESCOLLECTION = "wd.inferredTypesCollection";
	private static final String WD_INFERREDTYPESLINKS = "wd.inferredTypesLinks";
	private static final String WD_wikidataTitlePrefix = "wd.titlePrefix";
	// }}


	private Properties props;

	//{{ Getters for all properties
	public String getDbServerAddress() {
		return props.getProperty(DBSERVER_ADDRESS);
	}

	public int getDbServerPort() {
		return Integer.parseInt(props.getProperty(DBSERVER_PORT));
	}

	public String getWikipediaDbName() {
		return props.getProperty(DBSERVER_WIKIPEDIADBNAME);
	}
	
	public String getWikidataDbName(){
		return props.getProperty(DBSERVER_WIKIDATADBNAME);
	}

	public String getCollectionName_sent() {
		return props.getProperty(DBSERVER_SENTCOLLNAME);
	}

	public String getCollectionName_annot() {
		return props.getProperty(DBSERVER_ANNOTCOLLNAME);
	}

	public String getCollectionName_docs() {
		return props.getProperty(DBSERVER_DOCCOLLNAME);
	}

	public String getRequiredClasses() {
		return props.getProperty(WORKFLOW_REQUIREDCLASSES);
	}

	public String getDocColumn_DocId() {
		return props.getProperty(DOCUMENT_DOCID);
	}

	public String getDocColumn_Title() {
		return props.getProperty(DOCUMENT_TITLE);
	}

	public String getDocColumn_Content() {
		return props.getProperty(DOCUMENT_CONTENT);
	}

	public String getSentColumn_DocId() {
		return props.getProperty(SENTENCE_DOCID);
	}

	public String getSentColumn_senId() {
		return props.getProperty(SENTENCE_SENTID);
	}

	public String getSentColumn_start() {
		return props.getProperty(SENTENCE_STARTPOSITION);
	}

	public String getSentColumn_end() {
		return props.getProperty(SENTENCE_ENDPOSITION);
	}

	public String getSentColumn_text() {
		return props.getProperty(SENTENCE_TEXT);
	}

	public String getAnnotColumn_docId() {
		return props.getProperty(ANNOTATION_DOCID);
	}

	public String getAnnotColumn_senId() {
		return props.getProperty(ANNOTATION_SENTID);
	}

	public String getAnnotColumn_start() {
		return props.getProperty(ANNOTATION_STARTPOSITION);
	}

	public String getAnnotColumn_end() {
		return props.getProperty(ANNOTATION_ENDPOSITION);
	}

	public String getAnnotColumn_neClass() {
		return props.getProperty(ANNOTATION_ENTITYCLASS);
	}

	public String getAnnotColumn_confidence() {
		return props.getProperty(ANNOTATION_CONFIDENCE);
	}

	public String getOutput_fileDirectory() {
		return props.getProperty(OUTPUT_FILEDIRECTORY);
	}

	public String[] getOutput_fileName() {
		return props.getProperty(OUTPUT_FILENAME).split(",");
	}

	public String[] getOutput_testFileName() {
		return props.getProperty(OUTPUT_TESTFILENAME).split(",");
	}
	
	public boolean isCreateTest(){
		return getOutput_testFileName().length > 0 ? true : false;
	}

	public int[] getOutput_maxDocCount() {
		String[] temp = props.getProperty(OUTPUT_MAXDOCCOUNT).split(",");
		int[] sizes = new int[temp.length];
		for(int i = 0 ; i < temp.length ; ++i){
			sizes[i] = Integer.parseInt(temp[i]);
		}
		
		return sizes;
	}

	public boolean isWriteAllSentences() {
		String allSentences = props.getProperty(OUTPUT_WRITEALLSENTENCES);
		if (allSentences.equalsIgnoreCase("false")) {
			System.out.println("Empty sentences are excluded from the training data.");
			return false;
		} else {
			System.out.println("All sentences are added to the training data.");
			return true;
		}
	}

	public int getOpt_numOfAnnotPerSent() {
		return Integer.parseInt(props.getProperty(OPT_ANNOTPERSENT));
	}

	public float getOpt_confidenceLevel() {
		return Float.parseFloat(props.getProperty(OPT_CONFLEVEL));
	}
	
	public boolean getOpt_isUpperCase(){
		return props.getProperty(OPT_ISUPPERCASE).equalsIgnoreCase("true") ? true : false;
	}
		
	public int getNumberTestDocs(){
		return Integer.parseInt(props.getProperty(OUTPUT_TESTDOCSNUM));
	}
	
	public String getTestFilesDirectory(){
		return props.getProperty(OUTPUT_TESTFILEDIRECTORY);
	}
	

	public String getWdInferredtypescollection() {
		return props.getProperty(WD_INFERREDTYPESCOLLECTION);
	}

	public String getWdInferredtypeslinks() {
		return props.getProperty(WD_INFERREDTYPESLINKS);
	}
	
	public String getWikidataTitlePrefix() {
		return props.getProperty(WD_wikidataTitlePrefix);
	}

	//}}
	
	public LoadDataConfig(String configPath_) {
		readConfiguration(configPath_);
	}

	private void readConfiguration(String path) {
		props = new Properties();
		try {
			InputStream fis = new FileInputStream(path);
			FastBufferedInputStream is = new FastBufferedInputStream(fis);

			props.load(is);
			fis.close();
			is.close();
		} catch (IOException ex) {
			System.err.println("Could not load config file:");

			System.err.println(ex.getMessage());

			System.exit(1);
		}
	}
}
