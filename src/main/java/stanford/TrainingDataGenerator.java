package stanford;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.bson.Document;
import org.javatuples.Pair;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class TrainingDataGenerator {

	// {{Final fields
	private static final String DEFAULT_CONFIG = "conf/config.properties";
	// }}

	int totalAnnot = 0;
	BufferedWriter trainOutBuffer;
	BufferedWriter testOutBuffer;
	Document currAnnot = null;

	public void run(final String configPath_) {

		/*
		 * Load configurations, create LoadDataConfig object and use it's
		 * getters to access all config
		 */
		LoadDataConfig config = new LoadDataConfig(configPath_);

		/*
		 * Create database client
		 */
		MongoClient client = new MongoClient(config.getDbServerAddress(), config.getDbServerPort());

		/*
		 * Create a query to search wikidata documents
		 */
		String requiredClasses = config.getRequiredClasses();

		Pattern linksRegex = Pattern.compile("^dewiki:");
		long start = System.currentTimeMillis();
		BasicDBObject wikidataQuery = new BasicDBObject();
		if (!requiredClasses.isEmpty()) {
			BasicDBList desiredTypes = new BasicDBList();
			for (String cls : requiredClasses.split(",")) {
				desiredTypes.add(new BasicDBObject("instanceOf.0", cls));
			}
			wikidataQuery.append("$or", desiredTypes);
		}

		wikidataQuery.append(config.getWdInferredtypeslinks(), linksRegex);

		/*
		 * Create cursor for documents collection
		 */
		MongoCursor<Document> wikiData_cursor = createCursor(client, config.getWikidataDbName(),
				config.getWdInferredtypescollection(), wikidataQuery, 50000);

		List<String> wikiDocsIds = getWikiIds(wikiData_cursor, config, client);

		Collections.shuffle(wikiDocsIds);

		int sizeIdx = 0;
		boolean isTest = config.isCreateTest();
		for (int size : config.getOutput_maxDocCount()) {

			/*
			 * Create the output buffer. It's declared global because we need to
			 * create it only once, and keep using it, so we can't declare it
			 * inside any repetitive method.
			 */
			FileWriter trainFileStream = null;
			try {
				trainFileStream = new FileWriter(
						config.getOutput_fileDirectory() + "/" + config.getOutput_fileName()[sizeIdx]);
			} catch (IOException e) {

				e.printStackTrace();
			}
			trainOutBuffer = new BufferedWriter(trainFileStream);

			if (isTest) {

				if (sizeIdx < config.getOutput_testFileName().length) {

					FileWriter testFileStream = null;
					try {
						testFileStream = new FileWriter(
								config.getTestFilesDirectory() + "/" + config.getOutput_testFileName()[sizeIdx]);
					} catch (IOException e) {

						e.printStackTrace();
					}
					testOutBuffer = new BufferedWriter(testFileStream);
				} else {
					isTest = false;
				}

			}
			BufferedWriter outputBuffer;
			/*
			 * start processing the documents in the cursor.
			 */

			int trainCount = 0;
			int testCount = 0;
			int idx = 0;
			ThreadLocalRandom randomGen = ThreadLocalRandom.current();
			System.out.println("# Processing the filtered documents started...");
			System.out.println("# Number of aquired Ids: " + wikiDocsIds.size());
			while (idx < wikiDocsIds.size() && idx < size) {

				if (isTest) {
					float random = randomGen.nextFloat();
					if (random <= 0.02) {
						outputBuffer = testOutBuffer;
						++testCount;
					} else {
						outputBuffer = trainOutBuffer;
						++trainCount;
					}
					processDocument(wikiDocsIds.get(idx), config, client, outputBuffer);
				}
				else{
					processDocument(wikiDocsIds.get(idx), config, client, trainOutBuffer);
					++trainCount;
				}
				if ((idx + 1) % 1000 == 0) {
					System.out.printf("%s %d\n", "Number of processed documents so far: ", idx + 1);
				}
				++idx;

			}
			System.out.println("# Processing the documents finished...");

			long end = System.currentTimeMillis();
			System.out.printf("Time elapsed: %d%n", end - start);
			System.out.printf("Number of processed documents: %d%n", trainCount + testCount);
			System.out.printf("Number of training documents: %d%n", trainCount);
			System.out.printf("Number of testing documents: %d%n", testCount);
			System.out.printf("Number of total produced annotations: %d%n", totalAnnot);
			++sizeIdx;
		}
		wikiData_cursor.close();
		client.close();

	}

	// {{ Filtering the documents list from wikipedia articles that are
	// instances of ILL or DIS in wikidata.

	@SuppressWarnings("unchecked")
	private List<String> getWikiIds(MongoCursor<Document> wikidata_cursor, LoadDataConfig config, MongoClient client) {

		// Create the list that contains docId for all documents from wikidata
		// that have the required document class.
		List<String> checkedDocs = new ArrayList<String>();
		MongoDatabase wikipediaDB = client.getDatabase(config.getWikipediaDbName());

		MongoCollection<Document> collection = wikipediaDB.getCollection(config.getCollectionName_docs());
		System.out.println("# Aquiring document Ids started...");
		long start = System.currentTimeMillis();
		while (wikidata_cursor.hasNext()) {
			Document wikidataDoc = wikidata_cursor.next();
			List<String> links = (List<String>) wikidataDoc.get("links");
			String wikidataDocTitle = links.stream().filter(str -> str.startsWith(config.getWikidataTitlePrefix()))
					.findAny().get().substring(config.getWikidataTitlePrefix().length());
			Document wikipediaDoc = collection.find(new BasicDBObject(config.getDocColumn_Title(), wikidataDocTitle))
					.first();

			if (wikipediaDoc != null) {

				String docId = wikipediaDoc.getString(config.getDocColumn_DocId());
				checkedDocs.add(docId);
			}

		}
		long end = System.currentTimeMillis();
		System.out.println("# Aquiring document Ids finished...");
		System.out.printf("Aquiring Ids took: %d ms%n", end - start);
		return checkedDocs;

	}
	// }}

	// {{ A function to process each document,
	// called inside a loop over all documents
	// in the database from inside the run function
	private void processDocument(String docId, LoadDataConfig config, MongoClient client, BufferedWriter OutBuffer) {

		TokenizerFactory<Word> tokenizerFactory = PTBTokenizer.factory();
		/*
		 * this is to make sure to delete the non-tokenizable character without
		 * logging a warning. All options are: noneDelete, firstDelete,
		 * allDelete, noneKeep, firstKeep, allKeep.
		 */
		tokenizerFactory.setOptions("untokenizable=noneDelete");

		// {{ Creating annotations collection query,
		// annotations cursor and pass them to processSentence as parameters to
		// avoid
		// invoking the database for each sentence.
		BasicDBObject annotQuery = new BasicDBObject();
		annotQuery.put(config.getAnnotColumn_docId(), docId);
		annotQuery.put(config.getAnnotColumn_neClass(), new BasicDBObject("$exists", true));
		if (config.getOpt_confidenceLevel() > 0) {
			annotQuery.put(config.getAnnotColumn_confidence(),
					new BasicDBObject("$gte", config.getOpt_confidenceLevel()));
		}
		MongoCursor<Document> cursor_annot = createCursor(client, config.getWikipediaDbName(),
				config.getCollectionName_annot(), annotQuery, 0);

		// }}

		/*
		 * create sentences query, it's simple, contains the docId only. And
		 * create sentences cursor.
		 */
		BasicDBObject sentQuery = new BasicDBObject(config.getSentColumn_DocId(), docId);
		MongoCursor<Document> cursor_sent = createCursor(client, config.getWikipediaDbName(),
				config.getCollectionName_sent(), sentQuery, 0);

		/*
		 * After creating a cursor for all documents in the collection (which is
		 * done in run() method), we iterate over each document and: - Get a
		 * cursor for annotations of that document. And call processDocument
		 * method. - In processDocument method we get a cursor for all sentences
		 * of that document. - Tokenize each sentence in the sentences list and
		 * call processSentence method. - processSentence method applies a merge
		 * process between the tokens list and the corresponding part of the
		 * annotations cursor. And then it exports the text of each token along
		 * with its NE class. - processSentenceLite method does the same thing
		 * but with excluding sentences that don't have any annotations inside.
		 * - Using either methods is decided based on a boolean flag set in the
		 * configuration file. - In processSentenceLite method, a list of pairs
		 * is used locally and temporarily to store each token along with its NE
		 * class. - No map could be used because the tokens are not unique so
		 * they can't be used as keys. Note: the token position is calculated by
		 * adding its offset in the sentence to the sentence starting position
		 * within the document.
		 */
		if (cursor_annot.hasNext()) {
			currAnnot = cursor_annot.next();
		}
		while (cursor_sent.hasNext()) {
			Document sent = cursor_sent.next();
			String sentText = sent.getString(config.getSentColumn_text());
			int sentStart = sent.getInteger(config.getSentColumn_start());
			StringReader reader = new StringReader(sentText);
			Tokenizer<Word> tokenizer = tokenizerFactory.getTokenizer(reader);
			List<Word> tokens = tokenizer.tokenize();
			processSentenceLite(tokens, sentStart, cursor_annot, config, OutBuffer);
		}

		/*
		 * Closing the current cursor after each document.
		 */
		cursor_sent.close();
		cursor_annot.close();
	}
	// }}

	// {{ A function to process each sentence found in the document, this is
	// called from inside processDocument method.
	private void processSentenceLite(List<Word> tokens_, int sentStart_, MongoCursor<Document> cursor_annot_,
			LoadDataConfig config, BufferedWriter OutBuffer) {
		try {
			String formatStr = "%s \t %s %n";
			boolean inAnnotation = false;
			int numOfAnnot = 0;
			boolean isUpperCaseSet = config.getOpt_isUpperCase();

			ObjectArrayList<Pair<String, String>> oneSentenceAnnotations = new ObjectArrayList<Pair<String, String>>();
			for (int idx = 0; idx < tokens_.size(); ++idx) {
				Word currToken = tokens_.get(idx);
				String currTokenText = currToken.value();
				int curTokenStart = sentStart_ + currToken.beginPosition();
				int curTokenEnd = sentStart_ + currToken.endPosition();
				Pair<String, String> currPair;

				boolean isAnnotated;
				if (isUpperCaseSet) {
					isAnnotated = (currAnnot.getInteger(config.getAnnotColumn_start()) == curTokenStart
							&& Character.isUpperCase(currTokenText.codePointAt(0))) || inAnnotation;
				} else {
					isAnnotated = currAnnot.getInteger(config.getAnnotColumn_start()) == curTokenStart || inAnnotation;
				}

				if (isAnnotated) {

					String curClass = currAnnot.getString(config.getAnnotColumn_neClass()).substring(0, 3);
					String confidence = currAnnot.getString(config.getAnnotColumn_confidence());

					if (curClass.equalsIgnoreCase("PER") || curClass.equalsIgnoreCase("LOC")
							|| curClass.equalsIgnoreCase("ORG")) {
						currPair = new Pair<String, String>(currTokenText.trim(), curClass);
					} else if (confidence != null && Double.parseDouble(confidence) > 0.2) {

						currPair = new Pair<String, String>(currTokenText.trim(), "OTH");

					} else {
						currPair = new Pair<String, String>(currTokenText.trim(), "O");
					}
					oneSentenceAnnotations.add(currPair);

					if (curTokenEnd == currAnnot.getInteger(config.getAnnotColumn_end())) {
						inAnnotation = false;
						if (!(currPair.getValue1().equalsIgnoreCase("OTH"))) {
							++numOfAnnot;
						}
						if (cursor_annot_.hasNext()) {
							currAnnot = cursor_annot_.next();
						}
					} else if (curTokenEnd < currAnnot.getInteger(config.getAnnotColumn_end())) {
						inAnnotation = true;
					}

				} else {
					currPair = new Pair<String, String>(currTokenText.trim(), "O");
					oneSentenceAnnotations.add(currPair);

				}

			}

			// The following checks if the current sentence satisfy all
			// filtering criteria.
			if (numOfAnnot >= config.getOpt_numOfAnnotPerSent() && oneSentenceAnnotations.size() >= (numOfAnnot * 2)) {
				for (Pair<String, String> pair : oneSentenceAnnotations) {
					++totalAnnot;
					try {
						OutBuffer.write(String.format(formatStr, pair.getValue0(), pair.getValue1()));

					} catch (IOException e) {
						System.out.println("Error while writing results from current sentence buffer.");

					}
				}
				try {
					OutBuffer.write("\n");
					OutBuffer.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}

				oneSentenceAnnotations.clear();
			} else {
				oneSentenceAnnotations.clear();
			}
		} catch (NullPointerException e) {
			System.out.println(currAnnot);
			e.printStackTrace();
			System.exit(1);
		}
	}
	// }}

	// {{ createCursor is a generic method that can create a cursor for any
	// collection.
	private MongoCursor<Document> createCursor(MongoClient client, String dbName, String collectionName,
			BasicDBObject searchQuery_, int limit) {

		MongoDatabase mongoDB = client.getDatabase(dbName);
		MongoCollection<Document> collection = mongoDB.getCollection(collectionName);
		if (limit > 0) {
			return collection.find().limit(limit).iterator();
		} else {
			return collection.find(searchQuery_).iterator();
		}

	}
	// }}

	public static void main(String[] args) {
		String configPath = DEFAULT_CONFIG;
		if (args.length > 0) {
			configPath = args[0];
		}
		System.out.println("# Creating data generator using the configurations file at: " + configPath);
		TrainingDataGenerator gen = new TrainingDataGenerator();
		System.out.println("# Running the generator: ");
		gen.run(configPath);
	}
}